import pygame

def loop(state):
    screen = state["screen"]
    screen.fill([0, 0, 0])

    if state["winner"] == 1:
        player1wins = state["player1wins"]
        textpos = player1wins.get_rect()
        textpos.centerx = screen.get_rect().centerx
        textpos.centery = screen.get_rect().centery
        screen.blit(player1wins, textpos)
    else:
        player2wins = state["player2wins"]
        textpos = player2wins.get_rect()
        textpos.centerx = screen.get_rect().centerx
        textpos.centery = screen.get_rect().centery
        screen.blit(player2wins, textpos)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            state["running"] = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            pygame.event.wait()
            for i in range(9):
                state["fields"][i] = None

            state["level"] = "INTRO"




