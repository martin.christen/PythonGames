import pygame

def loop(state):
    screen = state["screen"]
    screen.fill([0, 0, 0])

    welcome = state["welcome"]
    textpos = welcome.get_rect()
    textpos.centerx = screen.get_rect().centerx
    textpos.centery = screen.get_rect().centery
    screen.blit(welcome, textpos)

    presstostart = state["presstostart"]
    textpos = presstostart.get_rect()
    textpos.centerx = screen.get_rect().centerx
    textpos.centery = screen.get_rect().centery + 40
    screen.blit(presstostart, textpos)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            state["running"] = False
        if event.type == pygame.MOUSEBUTTONUP:
            state["level"] = "PLAYER1"
            pygame.event.wait()

