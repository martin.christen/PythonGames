import pygame


def loop(state, player):
    screen = state["screen"]
    screen.fill([0, 0, 0])

    if state["level"] == "PLAYER1":
        nextlevel = "PLAYER2"
    else:
        nextlevel = "PLAYER1"

    # draw the Tic-Tac-Toe 3x3 field
    pos0 = (135,0)
    pos1 = (135,405)

    pos2 = (270,0)
    pos3 = (270,405)

    pygame.draw.line(screen, (127,127,127), pos0, pos1, 5)
    pygame.draw.line(screen, (127,127,127), pos2, pos3, 5)

    pos4 = (0,135)
    pos5 = (405,135)

    pos6 = (0,270)
    pos7 = (405, 270)

    pygame.draw.line(screen, (127, 127, 127), pos4, pos5, 5)
    pygame.draw.line(screen, (127, 127, 127), pos6, pos7, 5)

    fields = state["fields"]
    for y in range(0,3):
        for x in range(0,3):
            i = y*3+x
            startx = 135 * x
            starty = 135 * y
            if fields[i] == 1: # Player 1:
                pygame.draw.ellipse(screen, (0,255,0), [startx, starty, 135, 135], 5)
            elif fields[i] == 2:
                pygame.draw.line(screen, (255, 0, 0), [startx, starty], [startx+135, starty+135], 5)
                pygame.draw.line(screen, (255, 0, 0), [startx+135, starty], [startx, starty + 135], 5)

    if player == 1:
        player1up = state["player1up"]
        textpos = player1up.get_rect()
        textpos.centerx = screen.get_rect().centerx
        textpos.centery = 450
        screen.blit(player1up, textpos)
    elif player == 2:
        player2up = state["player2up"]
        textpos = player2up.get_rect()
        textpos.centerx = screen.get_rect().centerx
        textpos.centery = 450
        screen.blit(player2up, textpos)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            state["running"] = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            mx = event.pos[0]
            my = event.pos[1]
            pygame.event.wait()

            if mx>=0 and mx<135:
                if my>=0 and my<135:
                    if fields[0] == None:
                        fields[0] = player
                        state["level"] = nextlevel
                elif my>=135 and my<270:
                    if fields[3] == None:
                        fields[3] = player
                        state["level"] = nextlevel
                elif my>=270 and my<405:
                    if fields[6] == None:
                        fields[6] = player
                        state["level"] = nextlevel
            elif mx>=135 and mx<270:
                if my>=0 and my<135:
                    if fields[1] == None:
                        fields[1] = player
                        state["level"] = nextlevel
                elif my>=135 and my<270:
                    if fields[4] == None:
                        fields[4] = player
                        state["level"] = nextlevel
                elif my>=270 and my<405:
                    if fields[7] == None:
                        fields[7] = player
                        state["level"] = nextlevel
            elif mx>=270 and mx<405:
                if my>=0 and my<135:
                    if fields[2] == None:
                        fields[2] = player
                        state["level"] = nextlevel
                elif my>=135 and my<270:
                    if fields[5] == None:
                        fields[5] = player
                        state["level"] = nextlevel
                elif my>=270 and my<405:
                    if fields[8] == None:
                        fields[8] = player
                        state["level"] = nextlevel

    # check if there is a winner:
    state["winner"] = None
    if fields[0] == fields[1] == fields[2]:
        if fields[0] != None:
            state["winner"] = fields[0]
    if fields[3] == fields[4] == fields[5]:
        if fields[3] != None:
            state["winner"] = fields[3]
    if fields[6] == fields[7] == fields[8]:
        if fields[6] != None:
            state["winner"] = fields[6]
    if fields[0] == fields[3] == fields[6]:
        if fields[0] != None:
            state["winner"] = fields[0]
    if fields[1] == fields[4] == fields[7]:
        if fields[1] != None:
            state["winner"] = fields[1]
    if fields[2] == fields[5] == fields[8]:
        if fields[2] != None:
            state["winner"] = fields[2]
    if fields[0] == fields[4] == fields[8]:
        if fields[0] != None:
            state["winner"] = fields[0]
    if fields[2] == fields[4] == fields[6]:
        if fields[2] != None:
            state["winner"] = fields[2]

    if state["winner"] != None:
        state["level"] = "GAMEOVER"
