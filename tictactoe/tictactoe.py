import pygame
from pygame.locals import *
import intro
import player
import gameover

#-------------------------------------------------------------------------------
state = {
    "level" : "INTRO",          # current level/mode
    "running": True,            # True if game is still running
    "clock": None,              # clock object
    "screen": None,             # pygame screen
    "welcome": None,            # welcome text
    "presstostart": None,       # "insert coin" text
    "player1up": None,          # text: player 1 is up
    "player2up": None,          # text: player 2 is up
    "player1wins": None,        # text: player 1 wins
    "player2wins": None,        # text: player 2 wins
    "winner": None,
    "fields": [None,None,None,
               None,None,None,
               None,None,None]  # Game Fields
}
# set game state to intro screen
#-------------------------------------------------------------------------------

pygame.init()

width = 500
height = 500

screen=pygame.display.set_mode([width,height])
state["screen"] = screen

clock = pygame.time.Clock()
state["clock"] = clock

font = pygame.font.Font(None, 36)
state["font"] = font

welcome = font.render("Welcome", 1, (255, 255, 255))
state["welcome"] = welcome

presstostart = font.render("Click Mouse Button to Start", 1, (0, 255, 255))
state["presstostart"] = presstostart

player1up = font.render("Player 1", 1, (0, 127, 0))
state["player1up"] = player1up

player2up = font.render("Player 1", 1, (127, 0, 0))
state["player2up"] = player2up

player1wins = font.render("Player 1 wins", 1, (0, 127, 0))
state["player1wins"] = player1wins

player2wins = font.render("Player 2 wins", 1, (127, 0, 0))
state["player2wins"] = player2wins


while state["running"]:
    if state["level"] == "INTRO":
        intro.loop(state)
    elif state["level"] == "PLAYER1":
        player.loop(state, 1)
    elif state["level"] == "PLAYER2":
        player.loop(state, 2)
    elif state["level"] == "GAMEOVER":
        gameover.loop(state)
    else:
        print("something wrong with game state...")

    clock.tick(60)
    pygame.display.flip()



