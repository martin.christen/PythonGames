import pygame
from pygame.locals import *

# init pygame
pygame.init()

# create window
screen_width=640
screen_height=480
screen=pygame.display.set_mode([screen_width,screen_height])

# create a sprite
sprite = pygame.sprite.Sprite()               # create sprite
sprite.image = pygame.image.load("ball.png")  # load ball image
sprite.rect = sprite.image.get_rect()         # use image extent values
sprite.rect.topleft = (100,100)               # set position of top left corner


clock = pygame.time.Clock()
running = True

while running:
    screen.fill((255, 255, 255))   # fill background (white)

    screen.blit(sprite.image, sprite.rect)  # draw sprite

    pygame.display.flip()

    clock.tick(60)  # limit FPS

    # handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
        elif event.type == pygame.MOUSEMOTION:
            # event.pos is a tuple with mouse position:
            # sprite.rect.topleft = event.pos

            # set center:
            w = sprite.rect.size[0]
            h = sprite.rect.size[1]
            sprite.rect.topleft = (event.pos[0] - w/2, event.pos[1] - h/2)

            #or:
            #sprite.rect.center = (event.pos[0], event.pos[1])



