import pygame
from pygame.locals import *

pygame.init()

screen_width=640
screen_height=480
screen=pygame.display.set_mode([screen_width,screen_height])

red = 0
green = 0
blue = 0

clock = pygame.time.Clock()
running = True
velocity = [4,2]


image = pygame.image.load("sprite.png")  # load sprite (yes, this is a joke ;-), read more about sprites on wikipedia)
                                         # This image is 150x365
rect = image.get_rect()                  # use image extent values
rect.topleft = (220,70)                  # set position of top left corner
original_center = rect.center

angle = 0

while running:
    screen.fill((0, 0, 0))   # fill background

    rotated_image = pygame.transform.rotate(image, angle)
    rotated_rect = rotated_image.get_rect()
    #rotated_rect.center = original_center

    screen.blit(rotated_image, rotated_rect.topleft)

    clock.tick(20)  # limit to 20 FPS


    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
            elif event.key == K_RIGHT:
                angle += 5
            elif event.key == K_LEFT:
                angle -= 5