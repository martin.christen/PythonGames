# also check: https://www.pygame.org/docs/ref/transform.html#pygame.transform.scale


import pygame
from pygame.locals import *

pygame.init()

screen_width=640
screen_height=480
screen=pygame.display.set_mode([screen_width,screen_height])

red = 0
green = 0
blue = 0

clock = pygame.time.Clock()
running = True
velocity = [4,2]


image = pygame.image.load("sprite.png")  # load sprite (yes, this is a joke ;-), read more about sprites on wikipedia)
                                         # This image is 150x365
rect = image.get_rect()                  # use image extent values
rect.topleft = (220,70)                  # set position of top left corner
original_center = rect.center

scale = 1.0


while running:
    screen.fill((0, 0, 0))   # fill background

    scaled_image = pygame.transform.scale(image, (int(rect.width*scale), int(rect.height*scale)))  # or smoothscale
    scaled_rect = scaled_image.get_rect()
    scaled_rect.center = original_center

    screen.blit(scaled_image, scaled_rect.topleft)

    clock.tick(20)  # limit to 20 FPS


    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False
            elif event.key == K_RIGHT:
                scale += 0.05
                if scale>2.0:
                    scale = 2.0
            elif event.key == K_LEFT:
                scale -= 0.05
                if scale<0.1:
                    scale = 0.1