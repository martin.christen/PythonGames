

import pygame
from pygame.locals import *
import random

# pygame init
pygame.init()

# create window
screen_width=640
screen_height=480
screen=pygame.display.set_mode([screen_width,screen_height])

clock = pygame.time.Clock()
running = True
cnt = 0

rockets = []

while running:
    screen.fill((255, 255, 255))   # draw white background

    # draw rocket sprite
    for rocket in rockets:
        screen.blit(rocket.image, rocket.rect)
        rocket.rect.topleft = (rocket.rect.topleft[0], rocket.rect.topleft[1] + 1)

    pygame.display.flip()

    clock.tick(60)  # limit fps
    cnt += 1        # increase counter

    ###################################################################################################################
    if cnt>30: # a new rocket every 1/2 second
        cnt = 0
        rocketsprite = pygame.sprite.Sprite()               # create sprite
        rocketsprite.image = pygame.image.load("rocket.png")  # load ball image
        rocketsprite.rect = rocketsprite.image.get_rect()         # use image extent values
        rocketsprite.rect.topleft = (random.randint(10, screen_width-10),0)
        rockets.append(rocketsprite)

    ####################################################################################################################
    # Destroy Rockets
    newrockets = []
    for rocket in rockets:
        if rocket.rect.topleft[1]<=screen_height:
            newrockets.append(rocket)

    rockets = newrockets   # overwrite list to destroy old one
    ####################################################################################################################

    # handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                running = False



