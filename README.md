## EuroPython 2017: Creating 2D and 3D Games with Python

Martin Christen, martin.christen@fhnw.ch<br/>
University of Applied Sciences and Arts Northwestern Switzerland

### Outline 2D

* Creating 2D games, basic principles
* Game Loop / Rendering Loop
    * Events / User Interaction
* Drawing in 2D 
   * Drawing Shapes
   * 2D Vectors, Matrices
   * Translation, Rotation, Scale
* Our first game: Tic Tac Toe ("Xs and Os")
   * State Machine - what states ?
   * Level Management
   * Game AI, and now the computer plays
* The power of OOP for game development
   * Examples
* Create your own game!!??!


### Going 3D

* Modern OpenGL / Vulkan API
* GPU / Shaders
* Creating our first 3D app (including shaders)


### What's next ?

